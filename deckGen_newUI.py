def htmlGen(deck):
    buf = '<table>\n<tr>'
    deck['deck'].insert(0,'')
    runcount = 0
    for i in deck['deck']:
        if runcount == 0:
            buf = buf + f"<th>이름</th><th>카드 설명</th><th> </th><th>주사위 설명</th><th>등급</th>\n</tr>\n"
        else:
            color ='rgb(78, 77, 89)'
            if i['isFar']:
                color = 'rgb(0, 76, 71)'
            if i['isOnly']:
                color = 'rgb(76, 71, 0)'
            buf = buf + f'<tr style="background: {color}">\n'
            try:
                buf = buf + f"<td>{i['nameInCardInfo']}</td><td>{i['cardDesc']}</td><td>1</td><td>{i['diceDescs'][0]}</td><td>{i['rarity']}</td>\n"
            except IndexError:
                buf = buf + f"<td>{i['nameInCardInfo']}</td><td>{i['cardDesc']}</td><td>1</td><td></td><td></td>\n"
            if i['diceCount'] == 1:
                buf = buf + '</tr>\n'
            else:
                for j in range(i['diceCount']-1):
                    buf = buf + '<tr class="invisible">\n'
                    buf = buf + f"<td></td><td></td><td>{j+2}</td><td>{i['diceDescs'][j+1]}</td><td>{i['rarity']}</td>\n"
                    buf = buf + '</tr>\n'
        runcount += 1
    buf = buf + '</table>'
    return buf

def genCardFromNumber(jsonData={},cardId=''):
    for i in list(jsonData.keys()):
        for k in jsonData[i]: 
            if k['cardId'] == cardId:
                return k
    return None

'''
import json
import os
cardJsonData = json.load(open(os.getcwd()+'/cards.json',encoding='utf-8')) 
pageJsonData = json.load(open(os.getcwd()+'/pages.json',encoding='utf-8')) 
print(deckGen(cardJsonData,pageJsonData,['뜬소문','도시의별','불순물'],1,True,['slash']))
'''