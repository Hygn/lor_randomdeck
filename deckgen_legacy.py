import json
import os


chapterDict = {
    '기본': 'CardInfo_Basic',
    '뜬소문': 'CardInfo_ch1',
    '도시괴담': 'CardInfo_ch2',
    '도시전설': 'CardInfo_ch3',
    '도시질병': 'CardInfo_ch4',
    '도시악몽': 'CardInfo_ch5',
    '도시의별': 'CardInfo_ch6',
    '불순물': 'CardInfo_ch7'
}
chapterList = ['CardInfo_Basic', 'CardInfo_ch1', 'CardInfo_ch2', 'CardInfo_ch3',
               'CardInfo_ch4', 'CardInfo_ch5', 'CardInfo_ch6', 'CardInfo_ch7']
avgCostList = [12, 12, 13, 15, 15, 15, 15, 16]
avgLightRecoverList = [1, 1, 2, 2, 3, 3, 3]
avgDrawList = [1, 1, 2, 2, 3, 4, 5, 5]
chapterNum = [0,1,2,3,4,5,6,7]

def deckGen(jsonData,pageJsonData, chapters=['기본'], difficulty=0, name_kr=True):
    uniqueCard = {}
    notUniqueCard = {}
    lightCard = {}
    drawCard = {}
    import random
    intChapter = chapters
    if name_kr:
        for i in chapters:
            intChapter[chapters.index(i)] = chapterDict[i]
    intPageJsonData = {}
    for i in [x.lower().replace('cardinfo','EquipPage') for x in intChapter]:
        intPageJsonData.update({i: pageJsonData[i]})
    if difficulty == 0 or difficulty >= 3:
        usedPage = list(intPageJsonData.keys())
        random.shuffle(usedPage)
        usedPage = usedPage[0]
    else:
        usedPage = 'CardInfo_Basic'
        for i in intChapter:
            if chapterList.index(usedPage) < chapterList.index(i):
                usedPage = i
        usedPage = usedPage.lower().replace('cardinfo','EquipPage')
    usedPage = intPageJsonData[usedPage]
    random.shuffle(usedPage)
    usedPage = usedPage[0]
    intJsonData = {}
    for i in intChapter:
        intJsonData.update({i: jsonData[i]})
    for i in list(intJsonData.keys()):
        uniqueCard.update({i: []})
        notUniqueCard.update({i: []})
        lightCard.update({i: []})
        drawCard.update({i: []})
        for j in intJsonData[i]:
            # Todo: 전용책장 기능 추가 and 장거리책장 추가
            if j['rarity'] == 'Unique':
                if j['isOnly'] == False or str(j['cardId']) in usedPage['onlycard']:
                    uniqueCard[i].append(j)
            else:
                if j['isOnly'] == False or str(j['cardId']) in usedPage['onlycard']:
                    if usedPage['range'] == 'close':
                        if j['isFar'] == False:
                            notUniqueCard[i].append(j)
                    elif usedPage['range'] == 'far':
                        if j['isFar'] == True:
                            notUniqueCard[i].append(j)
                    else:
                        notUniqueCard[i].append(j)
        if uniqueCard[i] == []:
            uniqueCard.pop(i)
    #                  [랜덤,빛회복,코스트,드로우]
    difficultyDetail = []
    for i in range(4):
        if i+1 <= difficulty:
            difficultyDetail.append(False)
        else:
            difficultyDetail.append(True)
    isRandom = difficultyDetail[0]
    difficultyDetail = difficultyDetail[1:4]
    random.shuffle(difficultyDetail)
    deck = []
    if random.randint(0, 1) == 1 and len(list(uniqueCard)) != 0:
        # 예술등급카드 사용
        uniqueCardKeys = list(uniqueCard.keys())
        random.shuffle(uniqueCardKeys)
        selectedUniqueCard = uniqueCard[uniqueCardKeys[0]]
        random.shuffle(selectedUniqueCard)
        deck.append(selectedUniqueCard[0])
    if isRandom:
        # 랜덤덱
        while True:
            notUniqueCardKeys = list(notUniqueCard.keys())
            random.shuffle(notUniqueCardKeys)
            selectedNotUniqueCard = notUniqueCard[notUniqueCardKeys[0]]
            random.shuffle(selectedNotUniqueCard)
            if deck.count(selectedNotUniqueCard[0]) < 3:
                deck.append(selectedNotUniqueCard[0])
            if len(deck) >= 9:
                break
    else:
        # 난이도별 덱
        desiredLightRecover = 0
        try:
            intChapter.remove('CardInfo_Basic')
        except ValueError:
            pass
        for i in intChapter:
            if desiredLightRecover < avgLightRecoverList[chapterList.index(i) - 1]:
                desiredLightRecover = avgLightRecoverList[chapterList.index(i) - 1]
        desiredDraw = 0
        for i in intChapter:
            if desiredDraw < avgDrawList[chapterList.index(i) - 1]:
                desiredDraw = avgDrawList[chapterList.index(i) - 1]
        desiredCost = 0
        for i in intChapter:
            if desiredCost < avgCostList[chapterList.index(i) - 1]:
                desiredCost = avgCostList[chapterList.index(i) - 1]
        deckGenRunCount = 0
        while True:
            deckGenRunCount = deckGenRunCount + 1
            if deckGenRunCount % 180 == 0 and deckGenRunCount != 0:
                deck = []
                desiredLightRecover, desiredCost, desiredDraw = desiredLightRecover - \
                    1, desiredCost + 1, desiredDraw - 1
                random.shuffle(difficultyDetail)
                if difficulty == 0 or difficulty >= 3:
                    usedPage = list(intPageJsonData.keys())
                    random.shuffle(usedPage)
                    usedPage = usedPage[0]
                else:
                    usedPage = 'CardInfo_Basic'
                    for i in intChapter:
                        if chapterList.index(usedPage) < chapterList.index(i):
                            usedPage = i
                    usedPage = usedPage.lower().replace('cardinfo','EquipPage')
                usedPage = intPageJsonData[usedPage]
                random.shuffle(usedPage)
                usedPage = usedPage[0]
            if deckGenRunCount == 4096:
                return None
            if deckGenRunCount == 1024:
                difficultyDetail = [False,True,False]
            for i in range(3):
                cost = 0
                lightRecoveryCount = 0
                drawCount = 0
                for j in deck:
                    # 덱 빛회복 카운트
                    if j['isLightRecover']:
                        lightRecoveryCount += 1
                    # 덱 전체 드로우 수
                    drawCount = drawCount + j['drawCount']
                    # 덱 전체 코스트
                    cost = cost + j['cost']
                if difficultyDetail[i]:
                    for k in list(intJsonData.keys()):
                        lightCard.update({k: []})
                        drawCard.update({k: []})
                        for l in intJsonData[k]:
                            if l['isLightRecover']:
                                lightCard[k].append(l)
                            if l['isDraw']:
                                drawCard[k].append(l)
                    if i == 0 and desiredLightRecover >= lightRecoveryCount:
                        # 빛회복
                        leftLightCard = lightCard
                        # 남은 빛회복 카드 계산
                        for j in list(lightCard.keys()):
                            for k in lightCard[j]:
                                if k in deck:
                                    leftLightCard[j].remove(k)
                        # 카드 뽑기
                        lightCardKeys = list(leftLightCard.keys())
                        random.shuffle(lightCardKeys)
                        selectedLightCard = leftLightCard[lightCardKeys[0]]
                        random.shuffle(selectedLightCard)
                        try:
                            if deck.count(selectedLightCard[0]) < 3:
                                deck.append(selectedLightCard[0])
                        except IndexError:
                            pass
                    if i == 2 and desiredDraw >= drawCount:
                        # 드로우
                        leftDrawCard = drawCard
                        # 남은 드로우 카드 계산
                        for j in list(drawCard.keys()):
                            for k in drawCard[j]:
                                if k in deck:
                                    leftDrawCard[j].remove(k)
                        # 카드 뽑기
                        DrawCardKeys = list(leftDrawCard.keys())
                        random.shuffle(DrawCardKeys)
                        selectedDrawCard = leftDrawCard[DrawCardKeys[0]]
                        random.shuffle(selectedDrawCard)
                        try:
                            if deck.count(selectedDrawCard[0]) < 3:
                                deck.append(selectedDrawCard[0])
                        except IndexError:
                            pass
                else:
                    notUniqueCardKeys = list(notUniqueCard.keys())
                    random.shuffle(notUniqueCardKeys)
                    selectedNotUniqueCard = notUniqueCard[notUniqueCardKeys[0]]
                    random.shuffle(selectedNotUniqueCard)
                    if deck.count(selectedNotUniqueCard[0]) < 3:
                        deck.append(selectedNotUniqueCard[0])
            try:
                refCard = {}
                for j in list(intJsonData.keys()):
                    refCard.update({j: []})
                if leftDrawCard == leftLightCard == refCard or desiredDraw <= drawCount and desiredLightRecover <= lightRecoveryCount:
                    notUniqueCardKeys = list(notUniqueCard.keys())
                    random.shuffle(notUniqueCardKeys)
                    selectedNotUniqueCard = notUniqueCard[notUniqueCardKeys[0]]
                    random.shuffle(selectedNotUniqueCard)
                    if deck.count(selectedNotUniqueCard[0]) < 3:
                        deck.append(selectedNotUniqueCard[0])
            except UnboundLocalError:
                pass
            if len(deck) == 9:
                cost = 0
                drawCount, lightRecoveryCount, cost = 0, 0, 0
                for j in deck:
                    # 덱 빛회복 카운트
                    if j['isLightRecover']:
                        lightRecoveryCount += 1
                    # 덱 전체 드로우 수
                    drawCount = drawCount + j['drawCount']
                    # 덱 전체 코스트
                    cost = cost + j['cost']
                if difficultyDetail[1] and (desiredCost - cost) < 2 and (desiredCost - cost) > -4:
                    #print((desiredCost - cost))
                    break
                elif difficultyDetail[1]:
                    #print(cost)
                    deck = []
                elif difficultyDetail[1] == False and (desiredCost - cost) > 3 or (desiredCost - cost) < -5:
                    if difficultyDetail[0] == False and lightRecoveryCount == 0:
                        break
                    elif difficultyDetail[0] == False:
                        deck = []
                    else:
                        break
                    if difficultyDetail[2] == False and drawCount == 0:
                        break
                    elif difficultyDetail[2] == False:
                        deck = []
                    else:
                        break
                else:
                    deck = []
    deck = sorted(deck, key=lambda card: card['cost'])
    #print(deck)
    drawCount, lightRecoveryCount, cost = 0, 0, 0
    for j in deck:
        # 덱 빛회복 카운트
        if j['isLightRecover']:
            lightRecoveryCount += 1
        # 덱 전체 드로우 수
        drawCount = drawCount + j['drawCount']
        # 덱 전체 코스트
        cost = cost + j['cost']
    return {'core': usedPage, 'deck': deck}


def htmlGen(deck):
    try:
        corebook = deck['core']['nameLocalizedKR']
    except TypeError:
        return ''
    buf = f'<div class="coreBook">핵심책장: {corebook}</div>'
    buf = buf + '<table>\n'
    deck['deck'].insert(0,'')
    runcount = 0
    for i in deck['deck']:
        if runcount == 0:
            buf = buf + f"<th>이름</th><th>코스트</th><th>카드 설명</th><th> </th><th>주사위 설명</th><th>빛회복</th><th>드로우</th><th>등급</th><th>cardId</th>\n"
        else:
            color ='rgb(78, 77, 89)'
            if i['isFar']:
                color = 'rgb(0, 76, 71)'
            if i['isOnly']:
                color = 'rgb(76, 71, 0)'
            buf = buf + f'<tr style="background: {color}">\n'
            try:
                buf = buf + f"<td>{i['nameInCardInfo']}</td><td>{i['cost']}</td><td>{i['cardDesc']}</td><td>1</td><td>{i['diceDescs'][0]}</td>\
                <td>{i['isLightRecover']}</td><td>{i['drawCount']}</td><td>{i['rarity']}</td><td>{i['cardId']}</td>\n"
            except IndexError:
                buf = buf + f"<td>{i['nameInCardInfo']}</td><td>{i['cost']}</td><td>{i['cardDesc']}</td><td>1</td><td></td>\
                <td>{i['isLightRecover']}</td><td>{i['drawCount']}</td><td>{i['rarity']}</td><td>{i['cardId']}</td>\n"
            if i['diceCount'] == 1:
                buf = buf + '</tr>\n'
            else:
                for j in range(i['diceCount']-1):
                    buf = buf + '<tr class="invisible">\n'
                    if i['diceDescs'][j+1] == '':
                        i['diceDescs'][j+1] = '없음'
                    buf = buf + f"<td class=\"invisible\"></td><td class=\"invisible\"></td><td class=\"invisible\"></td><td>{j+2}</td><td>{i['diceDescs'][j+1]}\
                        </td><td class=\"invisible\"></td><td class=\"invisible\"></td><td class=\"invisible\"></td><td class=\"invisible\"></td>\n"
                    buf = buf + '</tr>\n'
        runcount += 1
    buf = buf + '</table>'
    return buf

def genDeckFromNumber(jsonData={},pageJsonData={},deckList=''):
    import urllib.parse
    tmp_list = []
    tmp_pageList = []
    deckbuf = []
    corebuf = ''
    corebook = deckList.split('||p')[0]
    try:
        check = urllib.parse.unquote(deckList.split('||c')[1])
    except IndexError:
        check = '{}'
    deckList = deckList.split('||p')[1].split('||c')[0].split(',')
    for i in list(jsonData.keys()):
        for j in jsonData[i]:
            tmp_list.append(j)
    for i in list(pageJsonData.keys()):
        for j in pageJsonData[i]:
            tmp_pageList.append(j)
    for i in tmp_pageList:
        if i['textId'] == corebook:
            corebuf = i
    for i in deckList:
        for j in tmp_list:
            if str(i) == j['cardId']:
                deckbuf.append(j)
    return [{'core':corebuf,'deck':deckbuf},check]


#jsonData = json.load(open(os.getcwd()+'/cards.json',encoding='utf-8')) 
#pageJsonData = json.load(open(os.getcwd()+'/pages.json',encoding='utf-8')) 
#deck = deckGen(jsonData,pageJsonData,['도시의별'],3)
#for i in deck:
#    print([i['isFar'],i['isOnly'],i['name'],i['cardId']])