chapterDict = {
    '기본': 'basic',
    '뜬소문': 'ch1',
    '도시괴담': 'ch2',
    '도시전설': 'ch3',
    '도시질병': 'ch4',
    '도시악몽': 'ch5',
    '도시의별': 'ch6',
    '불순물': 'ch7'
}
chapterList = ['basic', 'ch1', 'ch2', 'ch3', 'ch4', 'ch5', 'ch6', 'ch7']
actList = ['Penetrate', 'Slash', 'Hit', 'Guard', 'Evasion']
avgCostList = [18, 18, 18, 14, 13, 15, 15, 16]
avgLightRecoverList = [1, 1, 1, 2, 2, 3, 3, 4]
avgDrawList = [1, 1, 2, 2, 3, 4, 5, 5]
def deckGen(cardJsonData, pageJsonData, chapters, difficulty, name_kr, prefferedType=[]):
    import random
    uniqueCard = {}
    lightCard = {}
    drawCard = {}
    # 유일
    highlanderCard = {}
    # 우선적으로 사용할 카드
    priorCard = {}
    # 예술제외 모든카드
    notUniqueCard = {}
    # 한국어 이름 to 챕터 ID
    I_chapter = []
    # 함수 내부에서 사용하는 챕터 데이터 생성
    if name_kr:
        for i in chapters:
            I_chapter.append(chapterList.index(chapterDict[i]))
    else:
        for i in chapters:
            I_chapter.append(chapterList.index(i))
    sorted(I_chapter)
    difficultyDetail = []
    isRandom = False
    if difficulty == 0:
        isRandom = True
    else:
        for i in range(1,6):
            if i <= difficulty:
                difficultyDetail.append(False)
            else:
                difficultyDetail.append(True)
        difficultyDetail.pop(0)
        difficultyDetail.pop(-1)
    random.shuffle(difficultyDetail)
    # 핵심책장 선택
    if 0 < difficulty <= 3:
        # 가능한 강력한 핵심책장 선택
        usePage = 'EquipPage_'+chapterList[I_chapter[-1]]
        usePage = pageJsonData[usePage]
        random.shuffle(usePage)
        usePage = usePage[0]
    elif difficulty == 0 or difficulty == 4:
        #랜덤 핵심책장
        usePage = []
        for i in I_chapter:
            for j in pageJsonData['EquipPage_'+chapterList[i]]:
                usePage.append(j)
        random.shuffle(usePage)
        usePage = usePage[0]
    else:
        # 가능한 약한 핵심책장 선택
        usePage = 'EquipPage_'+chapterList[I_chapter[0]]
        usePage = pageJsonData[usePage]
        random.shuffle(usePage)
        usePage = usePage[0]
    for i in I_chapter:
        key = 'CardInfo_'+chapterList[i].replace('basic','Basic')
        uniqueCard.update({key:[]})
        notUniqueCard.update({key:[]})
        lightCard.update({key:[]})
        drawCard.update({key:[]})
        highlanderCard.update({key:[]})
        priorCard.update({key:[]})
        # 카드 분류
        for j in cardJsonData[key]:
            # 전용책장 부합 or 전용 아님
            if j['isOnly'] == False or j['cardId'] in usePage['onlycard']:
                if j['rarity'] == 'Unique':
                    #예술등급
                    uniqueCard[key].append(j)
                #단거리
                elif usePage['range'] == 'close':
                    notUniqueCard[key].append(j)
                    if j['isLightRecover'] and j['isFar'] == False:
                        lightCard[key].append(j)
                    if j['isDraw'] and j['isFar'] == False:
                        drawCard[key].append(j)
                    if j['isHighlander'] and j['isFar'] == False:
                        highlanderCard[key].append(j)
                    for k in prefferedType:
                        if k in j['act'] and j['isFar'] == False:
                            priorCard[key].append(j)
                            break
                    if difficulty == 1 and j['isHighlander']:
                        priorCard[key].append
                #장거리
                elif usePage['range'] == 'far':
                    notUniqueCard[key].append(j)
                    if j['isLightRecover'] and j['isFar']:
                        lightCard[key].append(j)
                    if j['isDraw'] and j['isFar']:
                        drawCard[key].append(j)
                    if j['isHighlander'] and j['isFar']:
                        highlanderCard[key].append(j)
                    for k in prefferedType:
                        if k in j['act'] and j['isFar']:
                            priorCard[key].append(j)
                            break
                #복합
                else:
                    notUniqueCard[key].append(j)
                    if j['isLightRecover']:
                        lightCard[key].append(j)
                    if j['isDraw']:
                        drawCard[key].append(j)
                    if j['isHighlander']:
                        highlanderCard[key].append(j)
                    for k in prefferedType:
                        if k in j['act']:
                            priorCard[key].append(j)
                            break
                    if difficulty == 1 and j['isHighlander']:
                        priorCard[key].append(j)
    deck = []
    statDetail = [0,0,0]
    if random.randint(0,1) == 1 or difficulty == 1:
        if difficulty == 1:
            useUniqueCard = uniqueCard['CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')]
        else:
            useUniqueCard = uniqueCard['CardInfo_' + chapterList[I_chapter[random.randint(0,len(I_chapter)-1)]].replace('basic', 'Basic')]
        random.shuffle(useUniqueCard)
        try:
            deck.append(useUniqueCard[0])
        except IndexError:
            pass
    runCount = 0
    allowRandom = False
    if difficulty == 1:
        try:
            useHighLanderCard = highlanderCard['CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')]
            deck.append(useHighLanderCard[random.randint(0,len(useHighLanderCard)-1)])
        except (ValueError, IndexError):
            pass
    while True:
        if runCount % 720 == 0 and runCount !=0:
            # 난이도(상세)/ 덱 초기화
            random.shuffle(difficultyDetail)
            deck = []
            # 핵심책장 재선택
            if 0 < difficulty <= 3:
                # 가능한 강력한 핵심책장 선택
                usePage = 'EquipPage_'+chapterList[I_chapter[-1]]
                usePage = pageJsonData[usePage]
                random.shuffle(usePage)
                usePage = usePage[0]
            elif difficulty == 0 or difficulty == 4:
                #랜덤 핵심책장
                usePage = []
                for i in I_chapter:
                    for j in pageJsonData['EquipPage_'+chapterList[i]]:
                        usePage.append(j)
                random.shuffle(usePage)
                usePage = usePage[0]
            else:
                # 가능한 약한 핵심책장 선택
                usePage = 'EquipPage_'+chapterList[I_chapter[0]]
                usePage = pageJsonData[usePage]
                random.shuffle(usePage)
                usePage = usePage[0]
        if runCount == 3600:
            allowRandom = True
        if runCount == 4800:
            for i in I_chapter:
                key = ('CardInfo_' + chapterList[i].replace('basic', 'Basic'))
                priorCard.update({key:[]})
        if runCount == 7200:
            return None
        runCount += 1
        if isRandom:
            key = list(notUniqueCard.keys())
            random.shuffle(key)
            key = key[0]
            useCard = notUniqueCard[key]
            random.shuffle(useCard)
            deck.append(useCard[0])
        else:
            for i in range(3):
                if len(deck) >= 9:
                    break
                #덱 스텟 계산
                statDetail = [0,0,0]
                for j in deck:
                    if j['isLightRecover']:
                        statDetail[0] = statDetail[0] + 1
                    if j['isDraw']:
                        statDetail[1] = statDetail[1] + j['drawCount']
                    statDetail[2] = statDetail[2] + j['cost']
                if difficultyDetail[i]:
                    if i == 0 and statDetail[0] <= avgLightRecoverList[I_chapter[-1]]:
                        # 빛회복
                        useLightCard = ['']
                        if difficulty == 1:
                            # 가능한 강력한 빛회복
                            key = 'CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')
                            useLightCard = lightCard['CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')]
                        # 3개이상 카드 삭제
                        for i in useLightCard:
                            if deck.count(i) >= 3:
                                useLightCard.remove(i)
                        if difficulty != 1 or useLightCard == []:
                            # 랜덤 빛회복
                            key = 'CardInfo_' + chapterList[I_chapter[random.randint(0,len(I_chapter)-1)]].replace('basic', 'Basic')
                            useLightCard = lightCard['CardInfo_' + chapterList[I_chapter[random.randint(0,len(I_chapter)-1)]].replace('basic', 'Basic')]
                        random.shuffle(useLightCard)
                        try:
                            for j in useLightCard:
                                if j in priorCard[key]:
                                    deck.append(j)
                                    break
                                elif priorCard[key] == []:
                                    deck.append(useLightCard[0])
                                    break
                        except IndexError:
                            pass
                    elif i == 1 and statDetail[1] <= avgDrawList[I_chapter[-1]]:
                        useDrawCard = ['']
                        if difficulty == 1:
                            key = 'CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')
                            useDrawCard = drawCard['CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')]
                        for i in useDrawCard:
                            if deck.count(i) >= 3:
                                useDrawCard.remove(i)
                        if difficulty != 1 or useDrawCard == []:
                            key = 'CardInfo_' + chapterList[I_chapter[random.randint(0,len(I_chapter)-1)]].replace('basic', 'Basic')
                            useDrawCard = drawCard['CardInfo_' + chapterList[I_chapter[random.randint(0,len(I_chapter)-1)]].replace('basic', 'Basic')]
                        random.shuffle(useDrawCard)
                        try:
                            for j in useDrawCard:
                                if j in priorCard[key]:
                                    deck.append(j)
                                    break
                                elif priorCard[key] == []:
                                    deck.append(useDrawCard[0])
                                    break
                        except IndexError:
                            pass
                    elif statDetail[1] >= avgDrawList[I_chapter[-1]] - 1 and statDetail[0] >= avgLightRecoverList[I_chapter[-1]] - 1 or allowRandom:
                        useCard = ['']
                        if difficulty == 1:
                            key = 'CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')
                            useCard = notUniqueCard['CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')]
                        for i in useCard:
                            if deck.count(i) >= 3:
                                useCard.remove(i)
                        if difficulty != 1 or useCard == []:
                            key = 'CardInfo_' + chapterList[I_chapter[random.randint(0,len(I_chapter)-1)]].replace('basic', 'Basic')
                            useCard = notUniqueCard['CardInfo_' + chapterList[I_chapter[random.randint(0,len(I_chapter)-1)]].replace('basic', 'Basic')]
                        random.shuffle(useCard)
                        try:
                            for j in useCard:
                                if j in priorCard[key]:
                                    deck.append(j)
                                    break
                                elif priorCard[key] == []:
                                    deck.append(useCard[0])
                                    break
                        except IndexError:
                            pass
                    else:
                        pass
                else:
                    key = list(notUniqueCard.keys())
                    random.shuffle(key)
                    key = key[0]
                    useCard = notUniqueCard[key]
                    random.shuffle(useCard)
                    if i == 0:
                        try:
                            for j in useCard:
                                if j in priorCard[key] and j not in lightCard[key]:
                                    deck.append(j)
                                    break
                                elif priorCard[key] == [] and j not in lightCard[key]:
                                    deck.append(useCard[0])
                                    break
                        except IndexError:
                            pass
                    elif i == 1:
                        try:
                            for j in useCard:
                                if j in priorCard[key] and j not in drawCard[key]:
                                    deck.append(j)
                                    break
                                elif priorCard[key] == [] and j not in drawCard[key]:
                                    deck.append(useCard[0])
                                    break
                        except IndexError:
                            pass
                    else:
                        try:
                            for j in useCard:
                                if j in priorCard[key]:
                                    deck.append(j)
                                    break
                                elif priorCard[key] == []:
                                    deck.append(useCard[0])
                                    break
                        except IndexError:
                            pass
                    
            if len(deck) >= 9:
                for i in deck:
                    if deck.count(i) >= 4:
                        for j in range(deck.count(i)-3):
                            deck.remove(i)
                # 하랜 계산
                highlander = False
                for i in deck:
                    if i['isHighlander']:
                        highlander = True
                        break
                if highlander and difficulty == 1:
                    T_deck = deck
                    for i in T_deck:
                        #강제 하랜
                        for j in range(T_deck.count(i)-1):
                            deck.remove(i)
                if highlander and difficulty >= 4:
                    try:
                        deck.append(deck[0])
                    except IndexError:
                        pass
                # 덱 길이 조절 (랜덤 x)
                if difficultyDetail[2]:
                    # 코스트 관리
                    if abs(avgCostList[I_chapter[-1]] - statDetail[2]) <= 2 and len(deck) == 9:
                        break
                    else:
                        deck = []
                elif len(deck) == 9:
                    if abs(avgCostList[I_chapter[-1]] - statDetail[2]) > 2:
                        break
                    else:
                        deck = []
                if difficulty == 1 and random.randint(0,3) != 3:
                    try:
                        useHighLanderCard = highlanderCard['CardInfo_' + chapterList[I_chapter[-1]].replace('basic', 'Basic')]
                        deck.append(useHighLanderCard[random.randint(0,len(useHighLanderCard)-1)])
                    except (ValueError, IndexError):
                        pass
        if len(deck) >= 9:
            break
    deck = sorted(deck, key=lambda card: card['cost'])
    return {'core': usePage, 'deck': deck}

def htmlGen(deck):
    try:
        corebook = deck['core']['nameLocalizedKR']
    except TypeError:
        return ''
    buf = f'<div class="coreBook">핵심책장: {corebook}</div>'
    buf = buf + '<table>\n'
    deck['deck'].insert(0,'')
    runcount = 0
    for i in deck['deck']:
        if runcount == 0:
            buf = buf + f"<th>이름</th><th>코스트</th><th>카드 설명</th><th> </th><th>유형</th><th>주사위 설명</th><th>빛회복</th><th>드로우</th><th>등급</th><th>cardId</th>\n"
        else:
            color ='rgb(78, 77, 89)'
            if i['isFar']:
                color = 'rgb(0, 76, 71)'
            if i['isOnly']:
                color = 'rgb(76, 71, 0)'
            buf = buf + f'<tr style="background: {color}">\n'
            try:
                buf = buf + f"<td>{i['nameInCardInfo']}</td><td>{i['cost']}</td><td>{i['cardDesc']}</td><td>1</td><td>{i['act'][0]}</td><td>{i['diceDescs'][0]}</td>\
                <td>{i['isLightRecover']}</td><td>{i['drawCount']}</td><td>{i['rarity']}</td><td>{i['cardId']}</td>\n"
            except IndexError:
                buf = buf + f"<td>{i['nameInCardInfo']}</td><td>{i['cost']}</td><td>{i['cardDesc']}</td><td>1</td><td></td><td></td>\
                <td>{i['isLightRecover']}</td><td>{i['drawCount']}</td><td>{i['rarity']}</td><td>{i['cardId']}</td>\n"
            if i['diceCount'] == 1:
                buf = buf + '</tr>\n'
            else:
                for j in range(i['diceCount']-1):
                    buf = buf + '<tr class="invisible">\n'
                    buf = buf + f"<td class=\"invisible\"></td><td class=\"invisible\"></td><td class=\"invisible\"></td><td>{j+2}</td><td>{i['act'][j+1]}</td><td>{i['diceDescs'][j+1]}\
                        </td><td class=\"invisible\"></td><td class=\"invisible\"></td><td class=\"invisible\"></td><td class=\"invisible\"></td>\n"
                    buf = buf + '</tr>\n'
        runcount += 1
    buf = buf + '</table>'
    return buf

def genDeckFromNumber(jsonData={},pageJsonData={},deckList=''):
    import urllib.parse
    tmp_list = []
    tmp_pageList = []
    deckbuf = []
    corebuf = ''
    corebook = deckList.split('||p')[0]
    try:
        check = urllib.parse.unquote(deckList.split('||c')[1])
    except IndexError:
        check = '{}'
    deckList = deckList.split('||p')[1].split('||c')[0].split(',')
    for i in list(jsonData.keys()):
        for j in jsonData[i]:
            tmp_list.append(j)
    for i in list(pageJsonData.keys()):
        for j in pageJsonData[i]:
            tmp_pageList.append(j)
    for i in tmp_pageList:
        if i['textId'] == corebook:
            corebuf = i
    for i in deckList:
        for j in tmp_list:
            if str(i) == j['cardId']:
                deckbuf.append(j)
    return [{'core':corebuf,'deck':deckbuf},check]

'''                    
import json
import os
cardJsonData = json.load(open(os.getcwd()+'/cards.json',encoding='utf-8')) 
pageJsonData = json.load(open(os.getcwd()+'/pages.json',encoding='utf-8')) 
print(deckGen(cardJsonData,pageJsonData,['뜬소문','도시의별','불순물'],1,True,['slash']))
'''