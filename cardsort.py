from re import T
from typing import Text
from bs4 import BeautifulSoup
import os
import json
jsonData = {}
cardList = []
for i in os.listdir(os.getcwd()+'/DropCard'):
    XMLd = open(f"{os.getcwd()}/DropCard/{i}",'r',encoding='utf-8').read()
    soup = BeautifulSoup(XMLd, features='lxml')
    for j in soup.find_all('card'):
        cardSoup = BeautifulSoup(str(j), features='lxml')
        cardList.append(str(cardSoup.find(text=True)))
#add basic card
for i in range(1,17): cardList.append(str(i))
runcnt = 0
for i in os.listdir(os.getcwd()+'/CardList'):
    XMLd = open(f"{os.getcwd()}/CardList/{i}",'r',encoding='utf-8').read()
    soup = BeautifulSoup(XMLd, features='lxml')
    if i.replace('.txt','',1)[0:12] == i.replace('.txt','',1):
        jsonData.update({i.replace('.txt','',1)[0:12]:[]})
    else:
        try:
            int(i.replace('.txt','',1)[-1])
        except ValueError:
            jsonData.update({i.replace('.txt','',1):[]})
    for j in soup.find_all('card'):
        if j['id'] in cardList:
            runcnt += 1
            cardSoup = BeautifulSoup(str(j), features='lxml')
            descs = []
            act = []
            diceRange = []
            try:
                for k in cardSoup.find_all('behaviour'):
                    act.append(k['detail'])
                    descs.append(k['desc'])
                    diceRange.append([int(k['min']),int(k['dice'])])
                for k in cardSoup.find_all('scriptdesc'):
                    descs.append(k.text)
            except:
                descs = ['','']
            isLightRecover = False
            isDraw = False
            isHighlander = False
            isFar = False
            drawCount = 0
            lightRecoverCount = 0
            for k in descs:
                if '빛' in k or '코스트' in k and '회복' in k:
                    isLightRecover = True
                    for l in k.replace(' ','').split('회복'):
                        try:
                            lightRecoverCount = lightRecoverCount + int(l[-1])
                        except:
                            pass
                if '뽑음' in k:
                    isDraw = True
                    for l in k.replace(' ','').split('장'):
                        try:
                            drawCount = drawCount + int(l[-1])
                        except:
                            pass
                if '유일' in k or '중복된' in k:
                    isHighlander = True
                if cardSoup.find('spec')['range'] == 'Far':
                    isFar = True
                if isDraw and drawCount == 0:
                    drawCount = 1
            print(f"dumping: {j['id']} : {cardSoup.find('name').text}")
            key = i.replace('.txt','',1)[0:12]
            try:
                int(i.replace('.txt','',1)[-1])
            except:
                key = i.replace('.txt','',1)
            jsonData[key].append({
                'cardId':j['id'],
                'artwork':cardSoup.find('artwork').text,
                'nameInCardInfo':cardSoup.find('name').text,
                'name':cardSoup.find('name').text,
                'rarity':cardSoup.find('rarity').text,
                'cost':int(cardSoup.find('spec')['cost']),
                'rarity':cardSoup.find('rarity').text,
                'diceCount':len(cardSoup.find_all('behaviour')),
                'diceDescs':descs[0:-1],
                'cardDesc':descs[-1],
                'isLightRecover':isLightRecover,
                'isDraw':isDraw,
                'drawCount':drawCount,
                'lightRecoverCount':lightRecoverCount,
                'isHighlander':isHighlander,
                'isOnly':'OnlyPage' in str(j),
                'isFar':isFar,
                'act':act,
                'diceRange':diceRange
            }) 
try:
    open(os.getcwd()+'/cards.json','w')
except:
    open(os.getcwd()+'/cards.json','a')
json.dump(jsonData,open(os.getcwd()+'/cards.json','w',encoding='utf-8'),ensure_ascii=False, indent=4)
print(f'successfully dumped {runcnt} cards')
#equipPage
jsonData = {}
cardList = []
for i in os.listdir(os.getcwd()+'/DropBook'):
    XMLd = open(f"{os.getcwd()}/DropBook/{i}",'r',encoding='utf-8').read()
    soup = BeautifulSoup(XMLd, features='lxml')
    for j in soup.find_all('dropitem'):
        cardSoup = BeautifulSoup(str(j), features='lxml')
        cardList.append(str(cardSoup.find(text=True)))
runcnt = 0
for i in range(1,1000): cardList.append(str(i))
#스포방지 검침책장 제거
cardList.remove("102")
localize = open(f"{os.getcwd()}/LocalizeBook/_Books.txt",'r',encoding='utf-8').read()
localize = BeautifulSoup(localize, features='lxml')
for i in os.listdir(os.getcwd()+'/EquipPage'):
    XMLd = open(f"{os.getcwd()}/EquipPage/{i}",'r',encoding='utf-8').read()
    soup = BeautifulSoup(XMLd, features='lxml')
    if i.replace('.txt','',1)[0:13] == i.replace('.txt','',1):
        jsonData.update({i.replace('.txt','',1)[0:13]:[]})
    else:
        try:
            int(i.replace('.txt','',1)[-1])
        except ValueError:
            jsonData.update({i.replace('.txt','',1):[]})
    for j in soup.find_all('book'):
        if j['id'] in cardList:
            runcnt += 1
            cardSoup = BeautifulSoup(str(j), features='lxml')
            onlycard = []
            range_ = 'close'
            for k in cardSoup.find_all('onlycard'):
                onlycard.append(k.text)
            if cardSoup.find('rangetype') == None:
                pass
            elif cardSoup.find('rangetype') == 'range':
                range_ = 'far'
            else: 
                range_ = 'hybrid'
            print(f"dumping: {j['id']} : {cardSoup.find('name').text}")
            key = i.replace('.txt','',1)[0:13]
            try:
                int(i.replace('.txt','',1)[-1])
            except:
                key = i.replace('.txt','',1)
            try:
                jsonData[key].append({
                    'name':cardSoup.find('name').text,
                    'textId':cardSoup.find('textid').text,
                    'nameLocalizedKR':localize.find('bookdesc', {'bookid':cardSoup.find('textid').text}).find('bookname').text,
                    'passiveDescs':list(map(str,[x.find(text=True) for x in list(localize.find('bookdesc', {'bookid':cardSoup.find('textid').text}).find_all('passive'))])),
                    'onlycard':onlycard,
                    'range':range_,
                    'rarity':cardSoup.find('rarity').text
                })
            except AttributeError:
                pass
try:
    open(os.getcwd()+'/pages.json','w')
except:
    open(os.getcwd()+'/pages.json','a')
json.dump(jsonData,open(os.getcwd()+'/pages.json','w',encoding='utf-8'),ensure_ascii=False, indent=4)
print(f'successfully dumped {runcnt} pages')