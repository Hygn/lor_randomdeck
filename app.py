from json.decoder import JSONDecodeError
from flask import Flask, Response, render_template, request
import mimetypes
import deckGen_refactored as deckgen
import deckGen_newUI as newUI
import json
import os
app = Flask(__name__)
root = 'lor_random/beta'
domain = 'projects.hygn.moe'
jsonData = json.load(open(os.getcwd()+'/cards.json',encoding='utf-8')) 
pageJsonData = json.load(open(os.getcwd()+'/pages.json',encoding='utf-8')) 
rarityVcolor = {
    'common':'#00bd16',
    'uncommon': '#007ebd',
    'rare': '#8300d4',
    'unique': '#f0f03e'
}
@app.route('/'+root,methods=['get'])
def indexGet():
    deck = ''
    card_ = []
    for i in range(9):
        card_.append('')
    return render_template('new_index.html', deck=deck, root=root, shareLink = '', check={}, card_=card_, rarityColor='#00bd16', core='')
@app.route('/'+root,methods=['post'])
def indexPost():
    chapter = []
    prefAct = []
    for i in deckgen.chapterList:
        try:
            request.form[i]
            chapter.append(i)
        except:
            pass
    for i in deckgen.actList:
        try:
            request.form[i]
            prefAct.append(i)
        except:
            pass
    try:
        deck = deckgen.deckGen(jsonData,pageJsonData,chapter,int(request.form['difficulty']),False,prefAct)
        check = {'diff_'+request.form['difficulty']:'checked'}
        for i in chapter:
            check.update({'card_'+i:'checked'})
        for i in prefAct:
            check.update({'act_'+i:'checked'})
        if deck == None:
            return render_template('index.html', deck='덱 생성이 너무 오래 걸립니다 다른 값으로 시도하십시오.', root=root, shareLink='', check=check)
        while deck['deck'] == []:
            deck = deckgen.deckGen(jsonData,pageJsonData,chapter,int(request.form['difficulty']),False)
        #gen number from deck
        corebook = deck['core']['textId']
        shareLink = f'http://{domain}/{root}/share/{corebook}||p'
        for i in deck['deck']:
            shareLink = shareLink + str(i['cardId']) + ','
        shareLink = shareLink[0:-1]+'||c'+json.dumps(check)
        core = deck['core']['nameLocalizedKR']
        card_ = []
        for i in deck['deck']:
            card_.append(i['cardId'])
        rarityColor = rarityVcolor[deck['core']['rarity'].lower()]
        deck = newUI.htmlGen(deck)
        return render_template('new_index.html', deck=deck, root=root, shareLink=f'{domain}/{root}/share/{deck}', check=check, card_=card_, rarityColor=rarityColor, core=core)
    except:
        card_ = []
        for i in range(9):
            card_.append('501001')
        return render_template('new_index.html', deck='값에 체크후 다시 시도해 주세요', root=root, shareLink = '', check={}, card_=card_, rarityColor='#00bd16', core='')
@app.route(f"/{root}/card/")
def cardNone():
        return render_template('card.html', illustPath='', cardName='', cardCost='', root=root, 
                               fontSize='0em', nameOffset='0%', rarity='')
@app.route(f"/{root}/card/<cardid>")
def card_(cardid):
    card_ = newUI.genCardFromNumber(jsonData,cardid)
    if card_ == None:
        return ''
    else:
        fontSize = '6vh'
        nameOffset = str(70/len(card_['name']))
        if len(card_['name']) == 1:
            nameOffset = '45'
        if len(card_['name']) >= 3:
            nameOffset = str(85/len(card_['name']))
        if len(card_['name']) > 7:
            fontSize = '3vh'
            nameOffset = str(50/len(card_['name']))
        return render_template('card.html', illustPath=card_['name'].replace('.','').replace(' ','\ '), cardName=card_['name'], cardCost=card_['cost'], root=root, 
                               fontSize=fontSize, nameOffset=nameOffset, rarity=card_['rarity'].lower())
@app.route(f'/{root}/<path>')
def path(path):
    if path == None:
        return ''
    try:
        mime = mimetypes.guess_type(path)[0]
        if mime == 'text/html':
            return render_template(path)
        elif mime != None:
            return Response(open(os.getcwd() +'/statics/'+ path,'rb').read(),mimetype=mime)
        #mimetype guess unavailable
        else:
            return Response(open(os.getcwd() +'/statics/'+ path,'rb').read())
    except FileNotFoundError:
        return Response('not found',status=404)
@app.route(f'/{root}/lor_illust/<path>')
def illust(path):
    if path == None:
        return ''
    try:
        mime = mimetypes.guess_type(path)[0]
        if mime == 'text/html':
            return render_template(path)
        elif mime != None:
            return Response(open(os.getcwd() +'/lor_illust/'+ path,'rb').read(),mimetype=mime)
        #mimetype guess unavailable
        else:
            return Response(open(os.getcwd() +'/lor_illust/'+ path,'rb').read())
    except FileNotFoundError:
        return Response('not found',status=404)
@app.route(f"/{root}/share/<decks>")
def share(decks):
    deck = decks
    card_ = []
    try:
        deck = deckgen.genDeckFromNumber(jsonData,pageJsonData,deck)
        for i in deck[0]['deck']:
            card_.append(i['cardId'])
        check = json.loads(deck[1])
    except (JSONDecodeError,TypeError,IndexError):
        check = {'core':{},'deck':[]}
    deck = newUI.htmlGen(deck[0])
    return render_template('new_index.html', deck=deck, root=root, shareLink=f'{domain}/{root}/share/{decks}', check=check, card_=card_)
# API
@app.route(f'/{root}/api/<path>', methods=['get','post'])
def api(path=None):
    if path == 'cardData':
        return Response(json.dumps(jsonData),mimetype='text/json')
    if path == 'pageData':
        return Response(json.dumps(pageJsonData),mimetype='text/json')
    if path == 'deck':
        if request.method == 'GET':
            return Response(json.dumps({'name': 'deckGen API', 
                                        'version':'0.2.4b', 
                                        'field':{'chapters': 'array',
                                                 'difficulty': 'integer',
                                                 'krName': 'bool',
                                                 'prefAct': 'array'}})
                            ,mimetype='text/json')
        else:
            form = request.get_json()
            deck = json.dumps(deckgen.deckGen(jsonData,pageJsonData,form['chapters'],int(form['difficulty']),bool(form['krName']),form['prefAct']))
            return Response(deck,mimetype='text/json')
    return ''