from json.decoder import JSONDecodeError
from flask import Flask, Response, render_template, request
import mimetypes
import deckGen_refactored as deckgen
import deckGen_newUI as newUI
import json
import os
app = Flask(__name__)
root = 'lor_random'
domain = 'projects.hygn.moe'
jsonData = json.load(open(os.getcwd()+'/cards.json',encoding='utf-8')) 
pageJsonData = json.load(open(os.getcwd()+'/pages.json',encoding='utf-8')) 
@app.route('/'+root,methods=['get'])
def indexGet():
    deck = ''
    return render_template('index.html', deck=deck, root=root, shareLink = '', check={})
@app.route('/'+root,methods=['post'])
def indexPost():
    chapter = []
    prefAct = []
    for i in deckgen.chapterList:
        try:
            request.form[i]
            chapter.append(i)
        except:
            pass
    for i in deckgen.actList:
        try:
            request.form[i]
            prefAct.append(i)
        except:
            pass
    try:
        deck = deckgen.deckGen(jsonData,pageJsonData,chapter,int(request.form['difficulty']),False,prefAct)
        check = {'diff_'+request.form['difficulty']:'checked'}
        for i in chapter:
            check.update({'card_'+i:'checked'})
        for i in prefAct:
            check.update({'act_'+i:'checked'})
        if deck == None:
            return render_template('index.html', deck='덱 생성이 너무 오래 걸립니다 다른 값으로 시도하십시오.', root=root, shareLink='', check=check)
        while deck['deck'] == []:
            deck = deckgen.deckGen(jsonData,pageJsonData,chapter,int(request.form['difficulty']),False)
        #gen number from deck
        corebook = deck['core']['textId']
        shareLink = f'http://{domain}/{root}/share/{corebook}||p'
        for i in deck['deck']:
            shareLink = shareLink + str(i['cardId']) + ','
        shareLink = shareLink[0:-1]+'||c'+json.dumps(check)
        deck = deckgen.htmlGen(deck)
        return render_template('index.html', deck=deck, root=root, shareLink=shareLink, check=check)
    except:
        return render_template('index.html', deck='값에 체크후 다시 시도해주십시오.', root=root, shareLink='', check={})
@app.route(f'/{root}/<path>')
def path(path):
    if path == None:
        return ''
    try:
        mime = mimetypes.guess_type(path)[0]
        if mime == 'text/html':
            return render_template(path)
        elif mime != None:
            return Response(open(os.getcwd() +'/statics/'+ path,'rb').read(),mimetype=mime)
        #mimetype guess unavailable
        else:
            return Response(open(os.getcwd() +'/statics/'+ path,'rb').read())
    except FileNotFoundError:
        return Response('not found',status=404)
@app.route(f"/{root}/share/<decks>")
def share(decks):
    deck = decks
    try:
        deck = deckgen.genDeckFromNumber(jsonData,pageJsonData,deck)
        check = json.loads(deck[1])
    except (JSONDecodeError,TypeError,IndexError):
        check = {'core':{},'deck':[]}
    deck = deckgen.htmlGen(deck[0])
    return render_template('index.html', deck=deck, root=root, shareLink=f'{domain}/{root}/share/{decks}', check=check)
# API
@app.route(f'/{root}/api/<path>', methods=['get','post'])
def api(path=None):
    if path == 'cardData':
        return Response(json.dumps(jsonData),mimetype='text/json')
    if path == 'pageData':
        return Response(json.dumps(pageJsonData),mimetype='text/json')
    if path == 'deck':
        if request.method == 'GET':
            return Response(json.dumps({'name': 'deckGen API', 
                                        'version':'0.2.4b', 
                                        'field':{'chapters': 'array',
                                                 'difficulty': 'integer',
                                                 'krName': 'bool',
                                                 'prefAct': 'array'}})
                            ,mimetype='text/json')
        else:
            form = request.get_json()
            deck = json.dumps(deckgen.deckGen(jsonData,pageJsonData,form['chapters'],int(form['difficulty']),bool(form['krName']),form['prefAct']))
            return Response(deck,mimetype='text/json')
    return ''